var Cleverbot = require('cleverbot-node');
var say = require('say');
var color = require('colors');
var random = require('random-world');
var rg = require('random-greetings')
var CONFIG = require('./conversation_options.json');

cleverbotA = new Cleverbot;
cleverbotB = new Cleverbot;
cleverbotA.configure({botapi: "a6e8c0239594ae4fd441467477240f7b"});
cleverbotB.configure({botapi: "a6e8c0239594ae4fd441467477240f7b"});

// Bot configurations
var showSynopsis	= CONFIG.showSynopsis;		// 	Wether or not to show the synopsis before the conversation
var wait 			= CONFIG.wait;  			//	Wether or not to wait for the tts to finish before continuing with another API call. WARNING: This will make the conversation significantlly slower
var speak 			= CONFIG.speak; 			// 	Wether or not to use the tts package
var chanceOfTitle 	= CONFIG.chanceOfTitle;		//	Percent chane that a bit will have a title
var lowestPitch 	= CONFIG.lowestPitch;		//	The lowest possible pitch assigned to a bot
var highestPitch	= CONFIG.highestPitch;		//	The highest possible pitch assigned to a bot

// Random names & title
var nameA = random.firstname();
var nameB = random.firstname();

if ((Math.random()*100) < chanceOfTitle) nameA = random.title() + " " +nameA;
if ((Math.random()*100) < chanceOfTitle) nameB = random.title() + " " +nameB;

// Random Colors
var colors = ['red','green','yellow','blue','magenta','cyan'];
var colorA = colors[Math.floor(Math.random()*colors.length)];
var colorB = colors[Math.floor(Math.random()*colors.length)];
if (colorB === colorA) colorB = colors[Math.floor(Math.random()*colors.length)];

// Random pitch
var pitchA = (Math.random()*(highestPitch-lowestPitch))+lowestPitch;
var pitchB = (Math.random()*(highestPitch-lowestPitch))+lowestPitch;

// Initialize count at 0
var count = 0;

// Set colors
color.setTheme({
	Acolor: colorA,
	Bcolor: colorB
})

// Show synopsis if enabled
if(showSynopsis){
	console.log(color.Acolor(nameA) + " and " + color.Bcolor(nameB)  + " just met.");
	console.log("Here is their conversation:\n");
}

// Start the conversation
converse(nameA, rg.greet(), pitchA, cleverbotB);

function converse(name, message, voice, cleverbot){
	if (speak){
		if(!wait){
			say.speak(message, null, voice);
			getResponse(message, cleverbot);
		} else {
			say.speak(message, null, voice, (err) => {
			getResponse(message, cleverbot);
			});
		}
	} else {
		getResponse(message, cleverbot);
	}
	if (name === nameA){
		console.log(name +": " + color.Acolor(message));
	} else {
		console.log(name +": " + color.Bcolor(message));
	}
}

function getResponse(message, cleverbot){
	cleverbot.write(message, function (response) {
		count++;
			if(count % 2 == 0){
				converse(nameA, response.output, pitchA, cleverbotB);
			} else {
				converse(nameB, response.output, pitchB, cleverbotA);
			}
	});
}
